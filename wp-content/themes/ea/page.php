<?php get_template_part('templates/head');?>
<!-- Feature Section -->
<div class="section" id="feature">
	<img class="arrow" src="assets/img/arrow-white.png" alt="arrow" />
	<div class="container">
		<div class="row">
			<div class="span12">
				<h1 class="white">
					We are a pressure group that seeks to represent the needs of the growing number of independent wealth creators in Britain.
				</h1>
				<h3>
					By coming together we hope to stand up for the mass entrepreneurial culture; wield our might and that of our members to guard against an 
					economy too focused in the needs of big business and restore Britain's faith in responsible capitalism.
				</h3>
			</div><!-- /.span12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.section -->

<!-- About Section -->
<div class="section" id="about">
	<span class="reverse-arrow"></span>
	<div class="container">
		<div class="row">
			<div class="span12">
				<h1>
					About Entrepreneurs Alliance
				</h1>
				<h3>
					The group was formed in November 2013 when its founding members recognised that there are times when a united front is necessary to protect 
					the needs of the next generation of British businesses.
				</h3>
			</div><!-- /.span12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.section -->

<!-- Who's involved Section -->
<div class="section" id="who">
	<h1>
		Who's involved?
	</h1>
	<div id="logos">
		<a href="http://tenentrepreneurs.org" title="The Entrepreneurs Network" target="_blank">
			<img src="assets/img/logo1.png" alt="The Entrepreneurs Network" />		
		</a>
		<a href="http://www.centreforentrepreneurs.org" title="Center for Entrepreneurs" target="_blank">
			<img src="assets/img/logo2.png" alt="Center for Entrepreneurs" />					
		</a>
		<a href="http://www.fpb.org" title="Forum of Private Business" target="_blank">
			<img src="assets/img/logo3.png" alt="Forum of Private Business" />					
		</a>
		<a href="https://www.enterprisenation.com" title="Enterpeise Nation" target="_blank">
			<img src="assets/img/logo4.png" alt="Enterpeise Nation" />					
		</a>
		<a href="http://www.fsb.org.uk" title="FSB" target="_blank">
			<img src="assets/img/logo5.png" alt="FSB" />					
		</a>
		<a href="http://www.nationalenterprisenetwork.org" title="National Enterprise Network" target="_blank">
			<img src="assets/img/logo6.png" alt="National Enterprise Network" />					
		</a>
		<a href="http://www.icaew.com" title="ICAEW" class="portrait" target="_blank">
			<img src="assets/img/logo7.png" alt="ICAEW" />					
		</a>
		<a href="http://www.bl.uk/bipc/" title="British Library" class="portrait" target="_blank">
			<img src="assets/img/logo8.png" alt="British Library" />					
		</a>
		<a href="http://www.schoolforstartups.co.uk" title="School for Startups" target="_blank">
			<img src="assets/img/logo9.png" alt="School for Startups" />					
		</a>
		<a href="http://www.ukbusinessforums.co.uk" title="UK Busines Forums" target="_blank">
			<img src="assets/img/logo10.png" alt="UK Busines Forums" />					
		</a>
		<a href="http://www.businesszone.co.uk/" title="Business Zone" target="_blank">
			<img src="assets/img/logo11.png" alt="Business Zone" />					
		</a>				
	</div><!-- /#logos -->
</div><!-- /.section -->

<!-- Media Links Section -->
<div class="section" id="media">
	<img class="arrow" src="assets/img/arrow-white.png" alt="arrow" />
	<div class="container">
		<div class="row">
			<h1 class="white">
				Media Links
			</h1>
			<!-- media link -->
			<div class="span3">
				<h4 class="white">Entrepreneurs now have their own pressure group - at last</h4>
				<p class="white">Published by City AM.<br />
				<a href="http://www.cityam.com/article/1384755125/entrepreneurs-now-have-their-own-pressure-group-last" target="_blank">Read article</a></p>
			</div>
			<!-- media link -->
			<div class="span3">
				<h4 class="white">Introducing Entrepreneurs' Alliance: Standing up for small business</h4>
				<p class="white">Published by Business Zone.<br />
				<a href="http://www.sift.com/news/introducing-entrepreneurs-alliance-standing-small-businesses" target="_blank">Read article</a></p>
			</div>
			<!-- media link -->
			<div class="span3">
				<h4 class="white">Entrepreneurs to get their own pressure group</h4>
				<p class="white">Published by The Startup Donut.<br />
				<a href="http://www.startupdonut.co.uk/news/startup/entrepreneurs-to-get-their-own-pressure-group" target="_blank">Read article</a></p>
			</div>
			<!-- media link -->
			<div class="span3">
				<h4 class="white">Open letter: From an 'Alliance of Entrepreneurs' to the Nation's Media</h4>
				<p class="white">Published by Enterprise Nation.<br />
				<a href="https://www.enterprisenation.com/blog/posts/open-letter-from-an-alliance-of-entrepreneurs-to-the-nation-s-media" target="_blank">Read article</a></p>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<!-- media link -->
			<div class="span3">
				<h4 class="white">It's time for entrepreneurs to make their numbers heard - for the benefit of the wider British economy</h4>
				<p class="white">Published by City AM.<br />
				<a href="http://www.cityam.com/article/1384747674/it-s-time-entrepreneurs-make-their-numbers-heard-benefit-wider-british-economy" target="_blank">Read article</a></p>
			</div>
			<!-- media link -->
			<div class="span3">
				<h4 class="white">Entrepreneurs for a better Britain</h4>
				<p class="white">Published by The Guardian.<br />
				<a href="http://www.theguardian.com/business/2013/nov/17/entrepreneurs-better-britain-business" target="_blank">Read article</a></p>
			</div>
			<!-- media link -->
			<div class="span3">
				<h4 class="white">"Entrepreneurs' Alliance" launches to stand up for wealth creators</h4>
				<p class="white">Published by Real Business.<br />
				<a href="http://realbusiness.co.uk/article/24750-entrepreneurs-alliance-launches-to-stand-up-for-wealth-creators" target="_blank">Read article</a></p>
			</div>
			<!-- media link -->
			<div class="span3">
				<h4 class="white">Entrepreneurs' Alliance Formed To 'Stand Up For The UK's Wealth Creators'</h4>
				<p class="white">Published by Huffington Post.<br />
				<a href="http://www.huffingtonpost.co.uk/2013/11/19/entrepreneurs-uk_n_4300685.html" target="_blank">Read article</a></p>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<!-- media link -->
			<div class="span3">
				<h4 class="white">Introducing The Entrepreneurs' Alliance: Standing up for small businesses</h4>
				<p class="white">Published by National Enterprise Network.<br />
				<a href="http://www.nationalenterprisenetwork.org/tag/entrepreneurs-alliance/" target="_blank">Read article</a></p>
			</div>
			<!-- media link -->
			<div class="span3">
				<h4 class="white">Entrepreneurs' Alliance Formed To Stand Up For The UK's Wealth Creators</h4>
				<p class="white">Published by Centre for Entrepreneurs.<br />
				<a href="http://www.centreforentrepreneurs.org/blog/entrepreneurs-alliance-formed-to-stand-up-for-the-UKs-wealth-creators" target="_blank">Read article</a></p>
			</div>
			<!-- media link -->
			<div class="span3">
				<h4 class="white">The Entrepreneurs' Alliance uncovered</h4>
				<p class="white">Published by Tax Assist Accountants.<br />
				<a href="http://www.taxassist.co.uk/resources/show-news/title/The-Entrepreneurs-Alliance-uncovered/id/900000263" target="_blank">Read article</a></p>
			</div>
		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.section -->

<!-- Register Section -->
<div class="section" id="register">
	<span class="reverse-arrow"></span>
	<div class="container">
		<div class="row">
			<div class="span4">
				<h2>Connect with us</h2>
				<a href="https://twitter.com/Entrep_Alliance" class="social" id="twitter" title="Entrepreneurs Alliance on Twitter" target="_blank">Entrepreneurs Alliance on Twitter</a>
				<a href="mailto:liz@enterprisenation.com" class="social" id="email" title="Tell someone about Entrepreneurs Alliance via email">Tell someone about Entrepreneurs Alliance via email</a>
			</div><!-- /.span3 -->
			<div class="span8">
				<h2>Register your interest</h2>
				<?php gravity_form(1, false, false, false, '', true, 12); ?>
				<img class="form-icon" src="assets/img/info.png" alt="Info" />
				<span class="form-description">Leave your email address with us for occasional updates on the Entrepreneurs' Alliance's activity. You can unsubscribe at any time and we won't spam you - ever.</span>
			</div><!-- /.span3 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.section -->

<?php get_template_part('templates/footer'); ?>