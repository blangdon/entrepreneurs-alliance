<?php get_template_part('templates/head');?>
	<div class="container">
		<div class="row">
			<div class="span3" id="sidebar">
				Cat menu can go here:
			</div><!-- /.span3 -->
			<div class="span9">
				<?php while (have_posts()) : the_post(); ?>
					<h1>
						<?php the_title(); ?>
					</h1>
					<div class="meta">
						<?php get_template_part('templates/entry-meta'); ?>
					</div>
					<?php 
						/*
						if ( has_post_thumbnail()) {
							$featured_thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
							$featured_thumb_url = $featured_thumb_url[0];
							the_post_thumbnail('medium'); 
						}
						*/ ?>
					<?php the_content(); ?>
					
				<?php endwhile; ?>
			</div><!-- /.span9 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
<?php get_template_part('templates/footer'); ?>