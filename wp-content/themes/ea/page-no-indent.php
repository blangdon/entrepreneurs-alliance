<?php
/*
Template Name: No Indent
*/
?>
<?php get_template_part('templates/head');?>

<div class="container">
	<div class="row no-indent">
		<div class="span12">
			<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
				<?php endwhile; ?>
		</div><!-- /.span12 -->	
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span12">
		<h1>.span</h1>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span12 bg-white">
		<p>12</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span11 bg-white">
		<p>11</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span10 bg-white">
		<p>10</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span9 bg-white">
		<p>9</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span8 bg-white">
		<p>8</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span7 bg-white">
		<p>7</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span6 bg-white">
		<p>6</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span5 bg-white">
		<p>5</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span4 bg-white">
		<p>4</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span3 bg-white">
		<p>3</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span2 bg-white">
		<p>2</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span1 bg-white">
		<p>1</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span12">
		<h1>.offset</h1>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span12 bg-white">
		<p>None</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span11 offset1 bg-white">
		<p>1</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span10 offset2 bg-white">
		<p>2</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span9 offset3 bg-white">
		<p>3</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span8 offset4 bg-white">
		<p>4</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span7 offset5 bg-white">
		<p>5</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span6 offset6 bg-white">
		<p>6</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span5 offset7 bg-white">
		<p>7</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span4 offset8 bg-white">
		<p>8</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span3 offset9 bg-white">
		<p>9</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span2 offset10 bg-white">
		<p>10</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
	<div class="row no-indent">
		<div class="span1 offset11 bg-white">
		<p>11</p>
		</div><!-- /.span1 -->
	</div><!-- /.row -->
</div><!-- /.container -->

<?php get_template_part('templates/footer'); ?>