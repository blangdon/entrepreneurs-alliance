<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- TITLE AND DESCRIPTION -->
    <title>
        <?php wp_title( '|', true, 'right' );

        // Add the blog name.
        bloginfo( 'name' );

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo( 'description', 'display' );
        if ( $site_description && ( is_home() || is_front_page() ) )
        echo " | $site_description"; ?>
    </title>
    <meta name="description" content="<?php
        if ( is_home() || is_front_page() ) {
            echo $site_description;
        } else if ( is_page() || is_single() ) {
            if ( have_posts() ) : while ( have_posts() ) : the_post();
            echo strip_tags(get_the_excerpt());
            endwhile; endif;
        }
    ?>">

    <!-- OPEN GRAPH -->
    <meta property='og:title' content='
        <?php wp_title( '|', true, 'right' );

        // Add the blog name.
        bloginfo( 'name' );

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo( 'description', 'display' );
        if ( $site_description && ( is_home() || is_front_page() ) )
        echo " | $site_description"; ?>
    '>
    <meta property='og:type' content='website'>
    <meta property='og:url' content='<?php echo get_permalink(); ?>'>
    <meta property='og:description' content='<?php
        if ( is_home() || is_front_page() ) {
            echo $site_description;
        } else if ( is_page() || is_single() ) {
            if ( have_posts() ) : while ( have_posts() ) : the_post();
            echo strip_tags(get_the_excerpt());
            endwhile; endif;
        }
    ?>'>
    <meta property='og:image' content='<?php echo get_template_directory_uri();?>/ico/bootstrap-apple-114x114.png'>

    <!-- MODERNIZR, JQUERY AND HTML5 SHIM -->
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- FAVICONS AND TOUCH ICONS -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/ico/favicon.ico">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri();?>/ico/bootstrap-apple-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri();?>/ico/bootstrap-apple-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri();?>/ico/bootstrap-apple-114x114.png">

    <!-- WP HEAD -->
    <?php wp_head(); ?>
    <!--[if IE ]>
        <script src="<?= get_template_directory_uri() ?>/assets/js/ie.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="<?= get_template_directory_uri() ?>/assets/js/ie8.js"></script>
        <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/assets/css/ie8.css">
    <![endif]-->
    
    <?php if (wp_count_posts()->publish > 0) : ?>
    <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
	<?php endif; ?>
	
	<!-- Typekit -->
	<script type="text/javascript" src="//use.typekit.net/kys6uoh.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

</head>

<body <?php body_class(); ?>>

	<!--[if lt IE 8]><div class="alert">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</div><![endif]-->
  
		<div id="head-bar">
			<h1>Welcome to Entrepreneurs' Alliance</h1>
		</div>