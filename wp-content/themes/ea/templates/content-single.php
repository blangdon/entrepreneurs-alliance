		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="span3" id="sidebar">
			<?php list_blog_categories(); ?>
		</div>
		<div class="span9">
			<?php while (have_posts()) : the_post(); ?>
				<h1>
					<?php the_title(); ?>
				</h1>
				<div class="meta">
					<?php get_template_part('templates/entry-meta'); ?>
				</div>
				<?php 
					/*
					if ( has_post_thumbnail()) {
						$featured_thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
						$featured_thumb_url = $featured_thumb_url[0];
						the_post_thumbnail('medium'); 
					}
					*/ ?>
				<?php the_content(); ?>
				<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
			<?php endwhile; ?>
			<?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
		</div>
	</div><!-- /.row -->
</div><!-- /.container -->