<?php
// UNDO ROOTS - this whole page exists to undo roots!

// #main CSS classes
function roots_main_class($sidebar=true) {
  if ($sidebar) {
    $class = 'span8';
  } else {
    $class = 'span12';
  }

  return $class;
}

// #sidebar CSS classes
function roots_sidebar_class() {
  return 'span4';
}


function roots_wrap_display_sidebar($display=null) {
    global $ROOTS_DISPLAY_SIDEBAR;
    if ( $display!==null ) {
        $ROOTS_DISPLAY_SIDEBAR = $display;
    }
    if ( !isset($ROOTS_DISPLAY_SIDEBAR) ) {
        $ROOTS_DISPLAY_SIDEBAR = true;
    }
    return $ROOTS_DISPLAY_SIDEBAR;
}
    
function roots_wrap_standard_before($sidebar=null) {
   $sidebar = roots_wrap_display_sidebar($sidebar);    
   get_template_part('templates/head');?>
   <body <?php body_class(); ?>>

  <!--[if lt IE 8]><div class="alert">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</div><![endif]-->

  <?php
    // Use Bootstrap's navbar if enabled in config.php
    if (current_theme_supports('bootstrap-top-navbar')) {
      get_template_part('templates/header-top-navbar');
    } else {
      get_template_part('templates/header');      
    }
  ?>

  <div id="wrap" class="container" role="document">
    <div id="content" class="row">
      <div id="main" class="<?php echo roots_main_class($sidebar); ?>" role="main"> 
<? }


function roots_wrap_standard_after() { ?>          
            </div>
          <?php if (roots_wrap_display_sidebar()) : ?>
          <aside id="sidebar" class="<?php echo roots_sidebar_class(); ?>" role="complementary">
            <?php get_template_part('templates/sidebar'); ?>
          </aside>
          <?php endif; ?>
        </div><!-- /#content -->
      </div><!-- /#wrap -->

      <?php get_template_part('templates/footer'); ?>
      
    </body>
    </html>
  
<?}
?>
