<?php

function doErrorReporting() {
    error_reporting(E_ALL);
    ini_set("display_errors", true);
}

/**
 * Theme Wrapper
 *
 * @link http://scribu.net/wordpress/theme-wrappers.html
 */

function roots_title() {
  if (is_home()) {
    if (get_option('page_for_posts', true)) {
      echo get_the_title(get_option('page_for_posts', true));
    } else {
      _e('Latest Posts', 'roots');
    }
  } elseif (is_archive()) {
    $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
    if ($term) {
      echo $term->name;
    } elseif (is_post_type_archive()) {
      echo get_queried_object()->labels->name;
    } elseif (is_day()) {
      printf(__('Daily Archives: %s', 'roots'), get_the_date());
    } elseif (is_month()) {
      printf(__('Monthly Archives: %s', 'roots'), get_the_date('F Y'));
    } elseif (is_year()) {
      printf(__('Yearly Archives: %s', 'roots'), get_the_date('Y'));
    } elseif (is_author()) {
      global $post;
      $author_id = $post->post_author;
      printf(__('Author Archives: %s', 'roots'), get_the_author_meta('display_name', $author_id));
    } else {
      single_cat_title();
    }
  } elseif (is_search()) {
    printf(__('Search Results for %s', 'roots'), get_search_query());
  } elseif (is_404()) {
    _e('File Not Found', 'roots');
  } else {
    the_title();
  }
}

// returns WordPress subdirectory if applicable
function wp_base_dir() {
  preg_match('!(https?://[^/|"]+)([^"]+)?!', site_url(), $matches);
  if (count($matches) === 3) {
    return end($matches);
  } else {
    return '';
  }
}

// opposite of built in WP functions for trailing slashes
function leadingslashit($string) {
  return '/' . unleadingslashit($string);
}

function unleadingslashit($string) {
  return ltrim($string, '/');
}

function add_filters($tags, $function) {
  foreach($tags as $tag) {
    add_filter($tag, $function);
  }
}

function is_element_empty($element) {
  $element = trim($element);
  return empty($element) ? false : true;
}


function get_attachment_id_from_src($image_src) {
    $image_src = preg_replace('/-[\dx]+(?=\.(jpg|jpeg|png|gif|pdf)$)/i', '', $image_src);
    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
    return $wpdb->get_var($query);
}

class OddOrEven {
    
    public static function reset($counter="oddOrEven") {
        $counter = "ODDOREVEN_".$counter;
        global $$counter;
        $$counter = "";
    }
    public static function get($counter="oddOrEven") {
        $counter = "ODDOREVEN_".$counter;
        global $$counter;
        if ( $$counter == "odd" ) {
            $$counter = "even";
        } else {
            $$counter = "odd";
        }
        return $$counter;
    }
    public static function isOdd($counter="oddOrEven"){
        $counter = "ODDOREVEN_".$counter;
        return oddOrEven($counter) == "odd";
    }
    public static function isEven($counter="oddOrEven"){
        $counter = "ODDOREVEN_".$counter;
        return oddOrEven($counter) == "odd";
    }
    
}

add_filter( 'template_include', 'var_template_include', 1000 );
function var_template_include( $t ){
    $GLOBALS['current_theme_template'] = basename($t);
    return $t;
}

function get_current_template( $echo = false ) {
    if( !isset( $GLOBALS['current_theme_template'] ) )
        return false;
    if( $echo )
        echo $GLOBALS['current_theme_template'];
    else
        return $GLOBALS['current_theme_template'];
}

function the_slug(){
  echo get_the_slug();
}
function get_the_slug($id=false){
  $slug = basename((!$id)?get_permalink():get_permalink($id));
  do_action('before_slug', $slug);
  $slug = apply_filters('slug_filter', $slug);
  do_action('after_slug', $slug);
  return $slug;
}

function setSidebar($sidebar) {
    global $SIDEBAR_ID;
    $SIDEBAR_ID = $sidebar_id;
}
function getSidebar() {
    global $SIDEBAR_ID;
    if ( !isset($SIDEBAR_ID) || !$SIDEBAR_ID ) {
        $SIDEBAR_ID = "primary";
    }
    return $SIDEBAR_ID;
}


/*
| -------------------------------------------------------------------
| Adding Breadcrumbs
| -------------------------------------------------------------------
|
| */
 function gs_breadcrumbs() {

  $delimiter = '<span class="divider">/</span>';
  $home = 'Home'; // text for the 'Home' link
  $before = '<li class="active">'; // tag before the current crumb
  $after = '</li>'; // tag after the current crumb

  if ( !is_home() && !is_front_page() || is_paged() ) {

    echo '<ul class="breadcrumb">';

    global $post;
    $homeLink = home_url();
    echo '<li><a href="' . $homeLink . '">' . $home . '</a></li> ' . $delimiter . ' ';

    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;

    } elseif ( is_day() ) {
      echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
      echo '<li><a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a></li> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;

    } elseif ( is_month() ) {
      echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;

    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;

    } elseif ( is_single() && !is_attachment() ) {
        
//      if ( get_post_type() == "journalissue" ) {
//            $journalHomepage = kc_get_option("options","specialpages","journalshomepage");            
//            echo '<li><a href="' . get_permalink($journalHomepage) . '">Journals</a></li> ' . $delimiter . ' ';
//            $issue = get_post_complete(get_the_ID());
//            echo '<li><a href="' . get_permalink($issue['journal']) . '">'.get_the_title($issue['journal']).'</a></li> ' . $delimiter . ' ';
//            echo $before . get_the_title() . $after;
//      } else if ( get_post_type() == "journalentry" ) {
//            $journalHomepage = kc_get_option("options","specialpages","journalshomepage");            
//            echo '<li><a href="' . get_permalink($journalHomepage) . '">Journals</a></li> ' . $delimiter . ' ';
//            $entry = get_post_complete(get_the_ID());            
//            $issue = get_post_complete($entry['journalissue']);
//            echo '<li><a href="' . get_permalink($issue['journal']) . '">'.get_the_title($issue['journal']).'</a></li> ' . $delimiter . ' ';
//            echo '<li><a href="' . get_permalink($entry['journalissue']) . '">'.get_the_title($entry['journalissue']).'</a></li> ' . $delimiter . ' ';            
//            echo $before . get_the_title() . $after;
//      } else if ( get_post_type() == "journal" ) {
//            $journalHomepage = kc_get_option("options","specialpages","journalshomepage");            
//            echo '<li><a href="' . get_permalink($journalHomepage) . '">Journals</a></li> ' . $delimiter . ' ';
//            echo $before . get_the_title() . $after;
//      } else 
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<li><a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a></li> ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        echo $before . get_the_title() . $after;
      }

    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;

    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<li><a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a></li> ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;

    } elseif ( is_page() && !$post->post_parent ) {
      echo $before . get_the_title() . $after;

    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;

    } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;

    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;

    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;

    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }

    if ( get_query_var('paged') && !is_404()) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page', 'bootstrapwp') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }

    echo '</ul>';

  }
} // end gs_breadcrumbs()