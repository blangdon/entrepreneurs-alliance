/* Author: Brad Langdon

*/

$(document).ready(function(){

	// Home page cycle posts
	$('#post-slideshow').cycle({ 
    	fx:     'fade', 
    	speed:  'fast', 
    	timeout: 3000000000000, 
    	pager:  '#nav',
    	pagerAnchorBuilder: function paginate(idx, el) {
			return '<a class="service' + idx + '" href="#" ></a>'; }
	});

});



