<?php get_template_part('templates/head');?>

<div class="container">
	<div class="row">
		<div class="span3" id="sidebar">
			<div class="item">
				<h3>Categories</h3>
				Cat menu can go here:
			</div>
		</div><!-- /.span3 -->
		<div class="span9">
			<?php $i=1; while (have_posts()) : the_post();
				$title = get_the_title();
				$excerpt = get_the_excerpt();
				$date = get_the_date('Y-m-d', '<p>', '</p>');
				$url = get_permalink();
				if ( has_post_thumbnail() ) {
					$featured_thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
					$featured_thumb_url = $featured_thumb_url[0];
				}else{
					$featured_thumb_url = get_template_directory_uri()."/assets/img/blog-bg.png";
				}
				if($i == 2){ $post_class = "no-margin-right"; }
				elseif($i == 3){ $post_class = "wide"; }
				else{ $post_class = ""; }
			?>
				<article id="post-<?php the_ID(); ?>" class="<?php echo $post_class; ?>">
					<span class="date"><?php echo $date; ?></span>
						<h3><a href="<?php echo $url; ?>"><?php echo $title; ?></a></h3>
						<?php the_excerpt(); ?>
					<a class="post-image" href="<?php the_permalink(); ?>" style="background: url(<?php echo $featured_thumb_url; ?>) center repeat;">
						
					</a>
				</article>
			<?php if($i==3){ $i=0; } $i=$i+1; endwhile; ?>
			
			<?php if ($wp_query->max_num_pages > 1) : ?>
				<?php if(function_exists('tw_pagination')) tw_pagination(); ?>
			<?php endif; ?>
		</div><!-- /.span9 -->
	</div><!-- /.row -->
</div><!-- /.container -->

<?php get_template_part('templates/footer'); ?>